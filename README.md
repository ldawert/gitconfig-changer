# gitconfig-changer

This repository contains scripts to quickly switch between different ~/.gitconfig files.

## Dependencies

The script [git_config](/git_config) heavily relies on [fzf](https://github.com/junegunn/fzf).
Also normal `find` command is replaced by [fd](https://github.com/sharkdp/fd) - which can be overwritten and is optional.

## Usage

To use these scripts copy both of them to a location stated in your `$PATH` variable.
You may need to change `config_dir` variable in [git_config](/git_config) file to suit your needs.

Example usage:
```plain
🐄 git cc
/Users/ldawert/.gitconfig is an existing symlink
Copying old config to /Users/ldawert/.gitconfig_old and deleting main conf file
Choose desired config:
<... SKIP INTERACTIVE FZF CHOICE ...>
Creating symlink for new config /Users/ldawert/.gitconfig_dir/.gitconfig-example
```
